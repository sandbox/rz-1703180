<?php
/**
 * @file Drush Scenario commands
 */

use Behat\Behat\Console\BehatApplication;
use Behat\Behat\Console\Command\BehatCommand;
use Symfony\Component\Console\Input\StringInput;

define('BEHAT_PHP_BIN_PATH', getenv('PHP_PEAR_PHP_BIN') ?: '/usr/bin/env php');
define('BEHAT_BIN_PATH',     __FILE__);
define('BEHAT_VERSION',      'DEV');

/**
 * Implementation of hook_drush_help().
 */
function scenario_drush_help($section) {
  switch ($section) {
    case 'meta:scenario:title':
      return dt('Scenario commands');
    case 'meta:scenario:summary':
      return dt('Commands for Behavior Driven Development with Drupal.');
  }
}

/**
 * Implementation of hook_drush_command().
 */
function scenario_drush_command() {
  $items['scenario-init'] = array(
    'description' => 'Create the necessary scaffolding in your project.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_CONFIGURATION,
    'aliases' => array(),
    'options' => array(),
    'examples' => array(
      'drush scenario-init' => 'Create the scaffolding in the default site folder.',
      'drush @local console' => 'Create the scaffolding in the site folder as determined by the drush alias.',
    ),
  );

  $items['scenario-behat'] = array(
    'strict-option-handling' => TRUE,
    'description' => 'Pass options and arguments directly to behat.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('behat', 'scenario-run'),
    'examples' => array(
      'drush @local scenario-behat' => 'Run behat using the configuration located in the current site\'s directory.',
    ),
  );

  return $items;
}

/**
 * Validate that a behat installation exists.  This function also does the basic
 * initialization that Behat requires, so this should always be run in the
 * validate hook for any Drush command that will invoke a BehatApplication.
 */
function _drush_scenario_validate_behat_installation($attempt_install = TRUE) {
  $installed = TRUE;

  if (file_exists(__DIR__ . '/vendor/autoload.php')) {
    require_once __DIR__ .'/vendor/autoload.php';
    if (!class_exists('Behat\Behat\Console\BehatApplication')) {
      $installed = FALSE;
    }
  }
  else {
    $installed = FALSE;
  }

  if (!$installed && $attempt_install) {
    _drush_scenario_install_dependencies();
    $installed = _drush_scenario_validate_behat_installation(FALSE);
  }

  return $installed;
}

/**
 * Install composer and dependencies.
 */
function _drush_scenario_install_dependencies() {
  $old_pwd = getcwd();
  chdir(__DIR__);

  drush_log(dt('Installing dependencies for Scenario.'), 'status');

  // Is composer installed?
  if (!file_exists(__DIR__ .'/composer.phar')) {
    drush_log('-- Installing composer for dependency management', 'status');
    $results = drush_shell_exec('curl -s http://getcomposer.org/installer | php');
    if (!$results) {
      $output = drush_shell_exec_output();
      return drush_set_error('SCENARIO_COMPOSER_INSTALL_ERROR', $output);
    }
  }

  // Install dependencies with composer.
  drush_log('-- Resolving dependencies, this can take a few minutes', 'status');
  if (drush_shell_exec('php composer.phar install') === FALSE) {
    $output = drush_shell_exec_output();
    return drush_set_error('SCENARIO_COMPOSER_INSTALL_ERROR', $output);
  }

  drush_log('-- Dependencies installed.', 'success');

  chdir($old_pwd);
}

/**
 * Validation callback for `drush scenario-init`
 */
function drush_scenario_init_validate() {
  if (!_drush_scenario_validate_behat_installation()) {
    return drush_set_error('SCENARIO_DEPENDENCIES_NOT_FOUND', dt('Dependencies for Scenario are not installed.'));
  }
}

/**
 * Command callback for `drush scenario-init`
 */
function drush_scenario_init() {
  $conf_path = realpath(conf_path());
  $dist_path = realpath(__DIR__ .'/dist');

  if (!drush_confirm(dt("Create Scenario scaffolding in @path?", array('@path' => $conf_path)))) {
    return;
  }

  if (_drush_scenario_init_recursive_copy($dist_path, $conf_path)) {
    drush_log(dt('Scenario scaffolding created in @path.', array('@path' => $conf_path)), 'success');
    $info = _drush_scenario_init_info($conf_path);
    drush_log($info, 'status');
  }
  else {
    drush_log(dt('Error creating Scenario scaffolding in @path.', array('@path' => $conf_path)), 'error');
  }
}

/**
 * Internal function called by drush_scenario_init.  Recursively copy a directory,
 * prompting for overwrite confirmation.
 */
function _drush_scenario_init_recursive_copy($src, $dest) {
  // all subdirectories and contents:
  if(is_dir($src)) {
    if (!drush_mkdir($dest, TRUE)) {
      return FALSE;
    }
    $dir_handle = opendir($src);
    while($file = readdir($dir_handle)) {
      if ($file != "." && $file != "..") {
        if (_drush_scenario_init_recursive_copy("$src/$file", "$dest/$file") !== TRUE) {
          return FALSE;
        }
      }
    }
    closedir($dir_handle);
  }
  elseif (!file_exists($dest) || drush_confirm(dt('@dest exists. Overwrite?', array('@dest' => $dest)))) {
    if (copy($src, $dest) !== TRUE) {
      return FALSE;
    }
  }

  return TRUE;
}

/**
 * Info string after initializing the scaffolding.
 */
function _drush_scenario_init_info($path) {
  $text = <<<EOD
Your Scenario scaffolding is located at {$path}.
- Behat configuration and profiles go in {$path}/behat.yml.
- features go in *.feature files in {$path}/features.
- bootstrap code is automatically included from {$path}/features/bootstrap.  You will find a FeaturesContext.php file that contains a subclass of Drupal\scenario\FeatureContext. This is where you will place any step definitions and hooks.

For further information about Behat configuration, features, steps and hooks see http://docs.behat.org/.
EOD;

  return $text;
}

/**
 * Command callback for `drush scenario-behat`
 */
function drush_scenario_behat_validate() {
  if (!_drush_scenario_validate_behat_installation()) {
    return drush_set_error('SCENARIO_DEPENDENCIES_NOT_FOUND', dt('Dependencies for Scenario are not installed.'));
  }
}

/**
 * Command callback for `drush scenario-behat`
 */
function drush_scenario_behat() {
  // Using the "strict-option-handling" we can get the options string as-is,
  // starting from after the command name.
  $cli_options = drush_get_original_cli_args_and_options();

  // Check for a --config option.
  $config = FALSE;
  foreach ($cli_options as $i => $option) {
    list ($key, $value) = explode('=', $option, 2);
    if ($key == '--config' || $key == '-c') {
      $config = TRUE;
      break;
    }
    // Dirty hack to kill the "Drush command terminated abnormally..." error
    // since the HelpProcessor immediately exits.
    if (in_array($key, array('--story-syntax', '--definitions', '-dl', '-di'))) {
      drush_set_context('DRUSH_EXECUTION_COMPLETED', TRUE);
    }

    // When invoked over a remote drush command, these path options get added after
    // the command options.  See drush_sitealias_set_alias_context().
    if (in_array($key, array('--drush-script', '--dump', '--dump-dir', '--include'))) {
      unset($cli_options[$i]);
    }
  }

  // No config found, add the config from the current site's folder.
  if (!$config) {
    $cli_options[] = sprintf('--config="%s/behat.yml"', conf_path());
  }

  $input = new StringInput(join(' ', $cli_options));

  $app = new BehatApplication(BEHAT_VERSION);
  $app->setAutoExit(FALSE);
  $app->run($input);
}

Feature: Visiting the homepage

  As an anonymous user
  I want to visit the homepage
  In order to see the site's content

  Scenario: Valid response from the homepage
    Given I am an anonymous user
     When I go to homepage
     Then the response status code should be 200

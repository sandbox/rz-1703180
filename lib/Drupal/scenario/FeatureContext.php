<?php

namespace Drupal\scenario;

use Behat\Symfony2Extension\Context\KernelAwareInterface;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Behat\Context\ClosuredContextInterface,
    Behat\Behat\Context\TranslatedContextInterface,
    Behat\Behat\Context\BehatContext,
    Behat\Behat\Exception\PendingException;
use Behat\Mink\Exception\DriverException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;

use Behat\Behat\Context\Step\Given;
use Behat\Behat\Context\Step\When;
use Behat\Behat\Context\Step\Then;

use Guzzle\Common\Event;

/**
 * Features context.
 */
class FeatureContext extends MinkContext {

  /**
   * Basic auth options.
   */
  protected $basic_auth = FALSE;

  /**
   * Initializes context.
   *
   * Every scenario gets its own context object.
   *
   * @param array $parameters.
   *   Context parameters (set them up through behat.yml or behat.local.yml).
   */
  public function __construct(array $parameters) {
    if (isset($parameters['basic_auth'])) {
      $this->basic_auth = $parameters['basic_auth'];
    }
  }

  /**
   * Inject the URL from Drush into the mink parameters as the base url.
   */
  public function setMinkParameters(array $parameters) {
    if ($drush_uri = drush_get_context('DRUSH_URI', FALSE)) {
      $uri_parts = parse_url($drush_uri);
      if (empty($uri_parts['scheme']) || empty($uri_parts['path'])) {
        throw new \Exception('The specified Drush URI must be a complete URI with a valid scheme and trailing slash.');
      }
      $parameters['base_url'] = $drush_uri;
    }

    return parent::setMinkParameters($parameters);
  }

  /**
   * Run before every scenario.
   *
   * @BeforeScenario
   */
  public function beforeScenario($event) {
    if (isset($this->basic_auth)) {
      $driver = $this->getSession()->getDriver();
      if ($driver instanceof Behat\Mink\Driver\Selenium2Driver) {
        // Continue if this is a Selenium driver, since this is handled in
        // locatePath().
      }
      else {
        // Setup basic auth.
        $this->getSession()->setBasicAuth($this->basic_auth['username'], $this->basic_auth['password']);
      }
    }

    // Add a listener to the Guzzle Client (part of the Goutte Driver) so that
    // we can fix the cookies before requests are sent.
    $driver = $this->getSession()->getDriver();
    if ($driver instanceof Behat\Mink\Driver\GoutteDriver) {
      $goutteClient = $driver->getClient();
      $guzzleClient = $goutteClient->getClient();
      $guzzleClient->getEventDispatcher()->addListener('request.before_send', array(__CLASS__, 'flattenGuzzleRequestCookies'));
    }
  }

  /**
   * Flatten multiple cookies into a single header since PHP doesn't correctly
   * handle multiple cookie headers.
   */
  public static function flattenGuzzleRequestCookies(Event $event) {
    $request = $event['request'];
    $cookies = $request->getHeader('Cookie');
    $request->setHeader('Cookie', "{$cookies}");
  }

  /**
   * Override MinkContext::locatePath() to work around Selenium not supporting
   * basic auth.
   */
  protected function locatePath($path) {
    $driver = $this->getSession()->getDriver();
    if ($driver instanceof Behat\Mink\Driver\Selenium2Driver && isset($this->basic_auth)) {
      // Add the basic auth parameters to the base url. This only works for
      // Firefox.
      $startUrl = rtrim($this->getMinkParameter('base_url'), '/') . '/';
      $startUrl = str_replace('://', '://' . $this->basic_auth['username'] . ':' . $this->basic_auth['password'] . '@', $startUrl);
      return 0 !== strpos($path, 'http') ? $startUrl . ltrim($path, '/') : $path;
    }
    else {
      $retval = parent::locatePath($path);
      return $retval;
    }
  }

}
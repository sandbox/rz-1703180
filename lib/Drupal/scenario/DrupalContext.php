<?php

namespace Drupal\scenario;

use Behat\Symfony2Extension\Context\KernelAwareInterface;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Behat\Context\ClosuredContextInterface,
    Behat\Behat\Context\TranslatedContextInterface,
    Behat\Behat\Context\BehatContext,
    Behat\Behat\Exception\PendingException;
use Behat\Mink\Exception\DriverException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;

use Behat\Behat\Context\Step\Given;
use Behat\Behat\Context\Step\When;
use Behat\Behat\Context\Step\Then;

use Guzzle\Common\Event;

/**
 * Features context.
 */
class DrupalContext extends FeatureContext {

  /**
   * Current authenticated user.
   *
   * A value of FALSE denotes an anonymous user.
   */
  public $loggedInUser = FALSE;

  /**
   * Keep track of all users that are created so they can easily be removed.
   */
  private $users = array();

  /**
   * Remove users created during this scenario, including the content they created.
   *
   * @AfterScenario
   */
  public function removeTestUsers($event) {
    // Remove any users that were created.
    if (!empty($this->users)) {
      foreach ($this->users as $account) {
        user_cancel(array(), $account->uid, 'user_cancel_delete');

        // I got the following technique here: http://drupal.org/node/638712
        $batch =& batch_get();
        $batch['progressive'] = FALSE;
        batch_process();
      }
    }
  }

  /**
   * Logs out the current user, if logged in.
   *
   * @Given /^I am an anonymous user$/
   * @Given /^I am logged out$/
   */
  public function logout() {
    // Verify the user is logged out.
    if ($this->loggedInUser) {
      $this->getSession()->visit($this->locatePath('/user/logout'));
      $this->loggedInUser = FALSE;
    }
  }

  /**
   * Creates and authenticates a user with the given role via Drush.
   *
   * @Given /^I am logged in as a user with the "([^"]*)" role$/
   */
  public function loginAsUserWithRole($role_name) {
    if (!($role = user_role_load_by_name($role_name))) {
      throw new \Exception("A role named \"$role_name\" was not found.");
    };

    // Check if a user with this role is already logged in.
    if ($this->loggedInUser && isset($this->loggedInUser->roles[$role->rid])) {
      return TRUE;
    }

    $account = $this->drupalCreateUser(array($role_name));
    $this->login($account->name, $account->pass_raw);

    return TRUE;
  }

  /**
   * Authenticates a user. This is both a Given and a utility function.
   *
   * @Given /^I am logged in as "([^"]*)" with the password "([^"]*)"$/
   */
  public function login($username, $password) {
    // Check if logged in.
    if ($this->loggedInUser && ($this->loggedInUser->name != $username)) {
      $this->logout();
    }

    $this->getSession()->visit($this->locatePath('/user'));

    $element = $this->getSession()->getPage()->find('css', '#user-login');
    $element->fillField('edit-name', $username);
    $element->fillField('edit-pass', $password);
    $submit = $element->findButton('Log in');
    if (empty($submit)) {
      throw new \Exception('No submit button at ' . $this->getSession()->getCurrentUrl());
    }

    // Log in.
    $submit->click();

    // If a logout link is found, we are logged in. While not perfect, this is
    // how Drupal SimpleTests currently work as well.
    if (!$this->getSession()->getPage()->findLink('Log out')) {
      throw new \Exception("Failed to log in as user \"{$username}\".");
    }

    // Load the user into loggedInUser and add the raw password.
    $this->loggedInUser = user_load_by_name($username);
    $this->loggedInUser->pass_raw = $password;
  }

  /**
   * Copied from drush_generate_password
   */
  protected function randomString($length = 10) {
    // This variable contains the list of allowable characters for the
    // password. Note that the number 0 and the letter 'O' have been
    // removed to avoid confusion between the two. The same is true
    // of 'I', 1, and 'l'.
    $allowable_characters = 'abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789';

    // Zero-based count of characters in the allowable list:
    $len = strlen($allowable_characters) - 1;

    // Declare the password as a blank string.
    $pass = '';

    // Loop the number of times specified by $length.
    for ($i = 0; $i < $length; $i++) {

      // Each iteration, pick a random character from the
      // allowable string and append it to the password:
      $pass .= $allowable_characters[mt_rand(0, $len)];
    }

    return $pass;
  }

  /**
   * Create a user with a given set of permissions.
   *
   * Adapted from DrupalWebTestCase.
   *
   * @param array $roles
   *   Array of role names to assign to user.
   *
   * @return object|false
   *   A fully loaded user object with pass_raw property, or FALSE if account
   *   creation fails.
   */
  protected function drupalCreateUser(array $roles = array()) {
    $rids = array();
    foreach ($roles as $name) {
      if (!($role = user_role_load_by_name($name))) {
        throw new \Exception("A role named \"$name\" was not found.");
      };
      $rids[$role->rid] = $role->rid;
    }

    // Create a user assigned to that role.
    $edit = array();
    $edit['name']   = $this->randomString(8);
    $edit['mail']   = $edit['name'] . '@example.com';
    $edit['pass']   = user_password();
    $edit['status'] = 1;
    $edit['roles'] = $rids;

    $account = user_save(drupal_anonymous_user(), $edit);

    if (empty($account->uid)) {
      throw new \Exception("User created with name \"{$edit[name]}\" and pass \"{$edit[pass]}\".");
    }

    // Add the raw password so that we can log in as this user.
    $account->pass_raw = $edit['pass'];

    $this->users[] = $account;

    return $account;
  }

  /**
   * Reset all data structures after having enabled new modules.
   *
   * This method is called by DrupalWebTestCase::setUp() after enabling
   * the requested modules. It must be called again when additional modules
   * are enabled later.
   *
   * Borrowed from DrupalWebTestCase.
   */
  protected function resetAll() {
    // Reset all static variables.
    drupal_static_reset();
    // Reset the list of enabled modules.
    module_list(TRUE);

    // Reset cached schema for new database prefix. This must be done before
    // drupal_flush_all_caches() so rebuilds can make use of the schema of
    // modules enabled on the cURL side.
    drupal_get_schema(NULL, TRUE);

    // Perform rebuilds and flush remaining caches.
    drupal_flush_all_caches();

    // Reload global $conf array and permissions.
    $this->refreshVariables();
    $this->checkPermissions(array(), TRUE);
  }

  /**
   * Refresh the in-memory set of variables. Useful after a page request is made
   * that changes a variable in a different thread.
   *
   * In other words calling a settings page with $this->drupalPost() with a changed
   * value would update a variable to reflect that change, but in the thread that
   * made the call (thread running the test) the changed variable would not be
   * picked up.
   *
   * This method clears the variables cache and loads a fresh copy from the database
   * to ensure that the most up-to-date set of variables is loaded.
   *
   * Borrowed from DrupalWebTestCase.
   */
  protected function refreshVariables() {
    global $conf;
    cache_clear_all('variables', 'cache_bootstrap');
    $conf = variable_initialize();
  }

}
